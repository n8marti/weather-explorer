#!/usr/bin/env python3

import numpy as np
import sys

from matplotlib import pyplot as plt

from api import API


def main():
    api = API()
    places = [
        'Dallas,TX',
        'Eynsham,UK',
        'Lucerne,Switzerland',
        'Bangui,CF',
        # 'Timbuktu,Mali',
    ]
    raw_data = {}
    for place in places:
        raw_data[place] = []
        params = {
            'location': place,
            'date1': 'last30days',
        }
        api.build_query(params)
        loc_data = api.get_query()
        for day in loc_data.get('days'):
            raw_data[place].append(day.get('feelslikemax'))

    raw_data['dates'] = [d.get('datetime') for d in loc_data.get('days')]

    # Build plot.
    x = np.arange(len(raw_data.get('dates')))
    for loc, temp in raw_data.items():
        if loc == 'dates':
            continue
        elif loc == 'Dallas,TX':
            color = 'gold'
        elif loc == 'Bangui,CF':
            color = 'green'
        elif loc == 'Eynsham,UK':
            color = 'blue'
        elif loc == 'Lucerne,Switzerland':
            color = 'red'
        plt.plot(x, temp, label=loc, c=color) # line graph

    # Annotate plot.
    plt.xticks(x, raw_data.get('dates'), rotation=40)
    plt.title("Max. Daily \"Feels like\" Temperature")
    plt.ylabel("Heat Index/Wind Chill (°F)")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
