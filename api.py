# REF:
# https://www.visualcrossing.com/resources/documentation/weather-api/timeline-weather-api/

import json
import urllib.request
import urllib.error

from secrets import api_key_visualcrossing


class API():
    def __init__(self):
        self.base_url = 'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/'
        self.api_key = api_key_visualcrossing
        self.default_params = {
            'location': 'Bangui,CF',
            'date1': 'today',
            'date2': '',
            'include': 'days',
            'elements': 'datetime,temp,tempmax,tempmin,feelslike,feelslikemax,feelslikemin,humidity',
            'unitGroup': 'us',
        }

    def build_query(self, user_params=dict()):
        params = self.default_params.copy() # initialize params
        for k, v in user_params.items():
            params[k] = v  # update with passed params

        # Add location to URL.
        self.api_query = f"{self.base_url}/{params.pop('location').replace(' ', '')}"
        # Add date leaves to URL.
        self.api_query += f"/{params.pop('date1').replace(' ', '')}"
        if len(params.get('date2')):
            self.api_query += f"/{params.pop('date2').replace(' ', '')}"
        self.api_query += "?"

        # Add parameters.
        for k, v in params.items():
            self.api_query += f"&{k}={v}"
        self.api_query += '&key=' + self.api_key
        return self.api_query

    def get_query(self):
        json_data = None
        try:
            r = urllib.request.urlopen(self.api_query)
            json_data = json.loads(r.read().decode('utf-8'))
        except urllib.error.HTTPError as e:
                print('Error code: ', e.code, e.read().decode())
        except urllib.error.URLError as e:
                print('Error code: ', e.code, e.read().decode())
        return json_data
