#!/usr/bin/env python3

# Ref:
#  wpc.ncep.noaa.gov/html/heatindex_equation.shtml

import math
import sys


def simple_heat_index(temp, humidity):
    return 0.5 * (temp + 61.0 + ((temp - 68.0) * 1.2) + (humidity * 0.094))

def general_heat_index(temp, humidity):
    heat_index = (
        -42.379 +
        2.04901523*temp +
        10.14333127*humidity -
        0.22475541*temp*humidity -
        0.00683783*temp**2 -
        0.05481717*humidity**2 +
        0.00122874*humidity*temp**2 +
        0.00085282*temp*humidity**2 -
        0.00000199*temp**2*humidity**2
    )
    return heat_index

def adjust_down(temp, humidity, heat_index):
    return heat_index - ((13 - humidity)/4) * math.sqrt((17 - math.fabs(temp - 95))/17)

def adjust_up(temp, humidity, heat_index):
    return heat_index + ((humidity - 85)/10)*((87 - temp)/5)

def average(items):
    return sum(items)/len(items)

def main():
    if len(sys.argv) >= 3:
        temp = float(sys.argv[1])
        humidity = float(sys.argv[2])

    # Step 1: basic calculation
    heat_index = simple_heat_index(temp, humidity)
    heat_index = average([heat_index, temp])
    if heat_index <= 80:
        print(heat_index)
        exit()

    # Step 2: general calculation
    heat_index = general_heat_index(temp, humidity)

    # Step 3: determine adjustments
    if humidity < 13 and temp > 80 and temp < 112:
        heat_index = adjust_down(temp, humidity, heat_index)
    elif humidity > 85 and temp > 80 and temp < 87:
        heat_index = adjust_up(temp, humidity, heat_index)

    print(int(round(heat_index, 0)))


if __name__ == '__main__':
    main()
