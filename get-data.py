#!/usr/bin/env python3

import sys

from api import API


def main():
    api = API()
    places = [
        'Bangui,CF',
        'Lamar,MO',
        'Tulsa,OK',
        'Eynsham,UK',
        'Basel,Switzerland',
    ]
    print("Temperature feels like:")
    for place in places:
        api.build_query({'location': place})
        data = api.get_query()
        if data is not None:
            print(f"{int(round(data.get('days')[0].get('feelslike'), 0))}°F in {data.get('resolvedAddress')}")

if __name__ == '__main__':
    main()
